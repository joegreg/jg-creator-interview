import React, { Component } from 'react';
import axios from 'axios';

class App extends Component {
  state = {
    repos: [],
    organisations: [],
  };

  queryGitHub = async event => {
    event.preventDefault();
    const username = this.username.value;
    const githubUrl = 'https://api.github.com';

    // 1. hit the github api for repos
    const reposPromise = axios.get(
      `${githubUrl}/users/${username}/repos?per_page=250`
    );

    // 2. hit the github api for orgs
    const orgsPromise = axios.get(`${githubUrl}/users/${username}/orgs`);

    const [repos, orgs] = await Promise.all([reposPromise, orgsPromise]);

    this.setState({
      repos: repos.data,
      organisations: orgs.data,
    });
    // 3. set the state with the results
  };

  renderRepos() {
    if (this.state.repos.length > 0) {
      return (
        <div className="repositories">
          <h2>Repos</h2>
          {this.state.repos.map(repo => (
            <div className="repo">
              <p>{repo.name}</p>
            </div>
          ))}
        </div>
      );
    }
  }

  renderOrgs() {
    if (this.state.organisations.length > 0) {
      return (
        <div className="organisations">
          <h2>Organisations</h2>
          {this.state.organisations.map(org => (
            <div className="org">
              <p>{org.login}</p>
            </div>
          ))}
        </div>
      );
    }
  }

  render() {
    return (
      <div className="App">
        <h1>Query GitHub</h1>

        <form onSubmit={this.queryGitHub}>
          <input type="text" ref={input => (this.username = input)} />
          <input type="submit" value="Submit" />
        </form>

        <div className="results">
          {this.renderRepos()}
          {this.renderOrgs()}
        </div>
      </div>
    );
  }
}

export default App;
